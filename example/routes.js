'use strict';

const joi = require('@hapi/joi');

const handler = request => request.payload  || {};
const LOREM_IPSUM = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';

module.exports = [{
  method: 'POST',
  path: '/echo',
  config: {
    handler,
    validate: {
      payload: joi.any(),
    },
    tags: ['api'],
    description: 'Simply replies what was send.',
    notes: LOREM_IPSUM,
  }
}, {
  method: 'GET',
  path: '/users',
  config: {
    handler,
    validate: {
      query: joi.object({
        $limit: joi.number()
          .integer().positive().description('Limits the number of users returned')
      })
    },
    tags: ['admin', 'api', 'users'],
    description: 'Get all users.',
    notes: LOREM_IPSUM,
  }
}, {
  method: 'POST',
  path: '/users',
  config: {
    handler,
    validate: {
      payload: joi.object({
        name: joi.string().alphanum().max(500).required().description('The full name'),
        email: joi.string().email().required().description('A valid email address'),
        themeColor: joi.string().hex().length(6).description('The users theme color'),
        group: {
          name: joi.string().alphanum().max(200),
          abbr: joi.string().token().max(10),
          memberNo: joi.number().integer().positive().description('This user\'s member number')
        }
      })
    },
    tags: ['api', 'users'],
    description: 'Create a new user',
    notes: LOREM_IPSUM,
  }
}, {
  method: 'GET',
  path: '/',
  handler: (req, h) => h.redirect('/docs/'),
}, {
  method: 'GET',
  path: '/internal',
  config: {
    isInternal: true,
    handler,
  }
}];
