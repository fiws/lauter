'use strict';

const hapi = require('@hapi/hapi');

const server = new hapi.server({ port: 8080 });

server.route(require('./routes'));
server.register([
  require('@hapi/inert'),
  {
    plugin: require('../lauter'),
    options: {
      version: '2.12.1',
      changelog: require('fs').readFileSync(__dirname + '/changelog.md', 'utf8'),
      groups: {
        users: {
          icon: 'groups',
        }
      }
    }
  }
]).then(async () => {
  await server.start();
  console.log('server started: http://localhost:8080/docs');
}).catch(console.error);
