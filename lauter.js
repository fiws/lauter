'use strict';

// helper libs
const Joi = require('joi');
const Hoek = require('@hapi/hoek');
const Call = new require('@hapi/call');
const Path = require('path');
const crypto = require('crypto');
const router = new Call.Router();

// Declare internals
const internals = {
  defaults: {
    endpoint: '/docs',
    endpointAuth: false,
    uiModule: 'lauter-ui',
    methodsOrder: ['get', 'head', 'post', 'put', 'patch', 'delete', 'trace', 'options'],
    filterRoutes: null,
  },
  options: Joi.object({
    groups: Joi.object(),
    title: Joi.string(),
    appKey: Joi.string(),
    url: Joi.string(),
    version: Joi.string(),
    changelog: Joi.string(),
    endpoint: Joi.string(),
    basePath: Joi.string(),
    auth: Joi.array().items(Joi.object()),
    endpointAuth: Joi.alternatives(Joi.object(), Joi.string()).allow(false),
    filterRoutes: Joi.func(),
  })
};


exports.register = (server, options) => {
  // get final settings
  const validateOptions = internals.options.validate(options);
  if (validateOptions.error) throw validateOptions.error;
  const settings = Hoek.clone(internals.defaults);
  Hoek.merge(settings, options);

  // url normalizing
  if (settings.endpoint[0] !== '/') settings.endpoint = `/${settings.endpoint}`;
  if (settings.endpoint.length > 1 && settings.endpoint[settings.endpoint.length - 1] === '/') {
    settings.endpoint = settings.endpoint.slice(0, -1);
  }

  // generate docs once on startup
  const docs = internals.docs(settings, server);
  const docsBuffer = new Buffer.from(JSON.stringify(docs));
  const docsHash = crypto.createHash('sha1').update(docsBuffer).digest('base64');

  server.dependency('@hapi/inert', server => {

    server.route({
      method: 'GET',
      path: settings.endpoint + '.json',
      config: internals.handler(settings, docsBuffer, docsHash),
    });

    server.route({
      method: 'GET',
      path: settings.endpoint + '/{path?}',
      config: internals.handler(settings, docsBuffer, docsHash),
    });
  });
};

internals.handler = (settings, docs, hash) => {
  const uiDir = Path.dirname(require.resolve(settings.uiModule));

  return {
    auth: settings.endpointAuth,
    handler(request, h) {

      if (request.url.pathname === settings.endpoint) {
        let prefix = settings.url || '';
        if (prefix.endsWith('/')) prefix = prefix.slice(0, -1);
        return h.redirect(prefix + settings.endpoint + '/');
      }
      const jsonOnly = request.route.fingerprint === settings.endpoint + '.json';
      if (jsonOnly || /^application\/json/.test(request.headers.accept)) {
        return h.response(docs).type('application/json').etag(hash);
      }
      if (!request.params.path) request.params.path = 'index.html';
      return h.file(request.params.path, { confine: uiDir });
    },
    plugins: {
      lauter: false
    }
  };
};

internals.docs = (settings, server) => {
  const table = server.table().filter(route => {
    // consider only non-internal, non-filtered, lauter-enabled routes
    return !route.settings.isInternal &&
      route.settings.plugins.lauter !== false &&
      route.method !== 'options' &&
      (!settings.filterRoutes || settings.filterRoutes({
        method: route.method,
        path: route.path,
        settings: route.settings
      }));
  }).sort((route1, route2) => {
    // sort them by path and in a certain order of http methods
    if (route1.path > route2.path) return 1;
    if (route1.path < route2.path) return -1;
    return settings.methodsOrder.indexOf(route1.method) - settings.methodsOrder.indexOf(route2.method);
  }).map(internals.getRouteData(server)); // reformat the routes

  const versions = table.filter(r => r.grouping.versionPrefix !== undefined)
    .map(r => r.grouping.versionPrefix)
    .filter((v, i, arr) => arr.indexOf(v) === i);

  const meta = {
    title: settings.title,
    appKey: settings.appKey,
    url: settings.url,
    version: settings.version,
    changelog: settings.changelog,
    auth: settings.auth,
  };
  if (meta.changelog) meta.changelogSha = crypto.createHash('sha1').update(meta.changelog).digest('hex');

  return {
    table,
    meta,
    versions,
    groups: settings.groups,
  };
};

internals.describePath = route => {
  let path = route.path;
  const desc = {};
  const versionMatch = path.match(/[v|V](ersion)?\d+/);
  if (versionMatch) {
    desc.versionPrefix = versionMatch[0];
    path = path.substring(desc.versionPrefix.length + 1);
  }
  desc.group = path.substring(1).split('/')[0] || 'root';
  desc.path = path;
  return desc;
};


internals.getRouteData = server => route => {
  const settings = route.settings;

  return {
    method: route.method.toUpperCase(),
    path: route.path,
    grouping: internals.describePath(route),
    analyzedPath: router.analyze(route.path),
    description: settings.description,
    notes: internals.processNotes(settings.notes),
    tags: settings.tags,
    auth: internals.processAuth(server, route),
    vhost: settings.vhost,
    cors: settings.cors,
    jsonp: settings.jsonp,
    pathParams: internals.describe(settings.validate.params),
    queryParams: internals.describe(settings.validate.query),
    payloadParams: internals.describe(settings.validate.payload),
    responseParams: internals.describe(settings.response.schema),
    statusSchema: internals.describeStatusSchema(settings.response.status),
  };
};


internals.describe = (params) => {
  if (params === null || typeof params !== 'object') return null;

  // convert to joi schema if needed
  // TODO: when is this not a schema..?
  let schema;
  if (Joi.isSchema(params, { legacy: true })) {
    schema = params;
  } else {
    Joi.compile(params, { legacy: true });
  }

  const description = internals.getParamsData(schema.describe());
  description.root = true;
  return description;
};

internals.describeStatusSchema = status => {
  const codes = Object.keys(status || {});
  if (!codes.length) return;

  const result = {};
  codes.forEach(code => result[code] = internals.describe(status[code]));
  return result;
};


internals.getParamsData = (param, name) =>  {
  // Detection of "false" as validation rule
  if (!name && param.type === 'object' && param.children && Object.keys(param.children).length === 0) {
    return { isDenied: true };
  }

  // Detection of conditional alternatives
  if (param.ref && param.is) {
    return {
      condition: {
        key: internals.formatReference(param.ref),
        value: internals.getParamsData(param.is, param.is.type)
      },
      then: param.then && internals.getParamsData(param.then, param.then.type),
      otherwise: param.otherwise && internals.getParamsData(param.otherwise, param.otherwise.type)
    };
  }

  let type;
  if (param.valids && param.valids.some(internals.isRef)) type = 'reference';
  else type = param.type;

  const data = {
    name,
    description: param.description,
    notes: internals.processNotes(param.notes),
    tags: param.tags,
    meta: param.meta,
    unit: param.unit,
    matches: param.matches,
    type,
    allowedValues: param.valids ? internals.getExistsValues(type, param.valids) : null,
    disallowedValues: param.invalids ? internals.getExistsValues(type, param.invalids) : null,
    examples: param.examples,
    peers: param.dependencies && param.dependencies.map(internals.formatPeers),
    target: type === 'reference' ? internals.getExistsValues(type, param.valids) : null,
    flags: param.flags && {
      allowUnknown: param.flags.allowUnknown,
      default: param.flags.default,
      encoding: param.flags.encoding, // binary specific
      insensitive: param.flags.insensitive, // string specific
      required: param.flags.presence === 'required',
      forbidden: param.flags.presence === 'forbidden',
      stripped: param.flags.strip,
      allowOnly: param.flags.allowOnly
    }
  };

  if (data.type === 'object') {
    let children = [];

    if (param.children) {
      const childrenKeys = Object.keys(param.children);
      children = children.concat(childrenKeys.map((key) => internals.getParamsData(param.children[key], key)));
    }

    if (param.keys) {
      const keysKeys = Object.keys(param.keys);
      children = children.concat(keysKeys.map((key) => internals.getParamsData(param.keys[key], key)));
    }

    if (param.patterns) {
      children = children.concat(param.patterns.map((pattern) => internals.getParamsData(pattern.rule, pattern.regex)));
    }

    data.children = children;
  }

  if (data.type === 'array' && param.items) {
    if (param.orderedItems) {
      data.orderedItems = param.orderedItems.map((item) => internals.getParamsData(item));
    }

    data.items = [];
    data.forbiddenItems = [];
    param.items.forEach((item) => {
      item = internals.getParamsData(item);
      if (item.flags && item.flags.forbidden) data.forbiddenItems.push(item);
      else data.items.push(item);
    });
  }

  if (data.type === 'alternatives') {
    if (param.alternatives) data.alternatives = param.alternatives.map((alternative) => internals.getParamsData(alternative));
  } else {
    data.rules = {};
    if (param.rules) {
      param.rules.forEach((rule) => {
        data.rules[internals.capitalize(rule.name)] = internals.processRuleArgument(rule);
      });
    }

    // If we have only one specific rule then set that to our type for brevity.
    const rules = Object.keys(data.rules);
    if (rules.length === 1 && !data.rules[rules[0]]) {
      data.rules = {};
      data.type = rules[0];
    }
  }

  return data;
};

internals.getExistsValues = (type, exists) => {
  const values = exists.filter((value) => {
    if (typeof value === 'string' && value.length === 0) return false;
    if (type === 'number' && Math.abs(value) === Infinity) return false;
    return true;
  }).map(value => {
    if (internals.isRef(value)) return internals.formatReference(value);
    return JSON.stringify(value);
  });

  return values.length ? values : null;
};

internals.capitalize = string => string.charAt(0).toUpperCase() + string.slice(1);

internals.formatPeers = condition => {
  if (condition.key) {
    return `Requires ${condition.peers.join(', ')} to ${condition.type === 'with' ? '' : 'not '}be present when ${condition.key} is.`;
  }
  return `Requires ${condition.peers.join(` ${condition.type} `)}.`;
};


internals.isRef = ref => typeof ref === 'string' && /^(ref|context):.+/.test(ref);

internals.formatReference = ref => {
  if (ref.path) {
    // TODO: is this correct, no idea
    return '$' + ref.path.join('.');
  }
  if (ref.startsWith('ref:')) return ref.substr(4);
  return '$' + ref.substr(8);
};

internals.processRuleArgument = function (rule) {
  const arg = rule.arg;
  if (rule.name === 'assert') {
    return {
      key: internals.formatReference(arg.ref),
      value: internals.getParamsData(arg.schema)
    };
  } else if (internals.isRef(arg)) {
    return { ref: internals.formatReference(arg) };
  }

  return arg || '';
};

internals.processNotes = notes => !notes ? undefined : !Array.isArray(notes) ? [notes] : notes;

internals.processAuth = (server, route) => server.auth.lookup(route);

exports.pkg = require('./package.json');
